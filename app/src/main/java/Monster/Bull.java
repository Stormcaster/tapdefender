package Monster;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by PrangerZ54 on 5/30/2016 AD.
 */
public class Bull extends Monster {

    public Bull() {
        name = "Blue Mushroom";
        damage = 5;
        curHp = maxHp = 20;
        img = R.drawable.mon_bull;
        bounty = 20;
    }
}
