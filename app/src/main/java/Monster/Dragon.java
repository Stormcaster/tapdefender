package Monster;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by PrangerZ54 on 5/30/2016 AD.
 */
public class Dragon extends Monster {

    public Dragon() {
        name = "Dragon";
        damage = 60;
        curHp = maxHp = 100;
        img = R.drawable.boss_3hdragon;
        bounty = 80;
    }
}
