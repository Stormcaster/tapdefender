package Monster;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by PrangerZ54 on 5/30/2016 AD.
 */
public class IceKnight extends Monster {

    public IceKnight() {
        name = "Ice Knight";
        damage = 50;
        curHp = maxHp = 80;
        img = R.drawable.monster_iceknight;
        bounty = 40;
    }
}
