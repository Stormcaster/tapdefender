package Monster;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by PrangerZ54 on 5/30/2016 AD.
 */
public class Dodo extends Monster {

    public Dodo() {
        name = "Dodo";
        damage = 5;
        curHp = maxHp = 10;
        img = R.drawable.mon_dodo;
        bounty = 5;
    }
}
