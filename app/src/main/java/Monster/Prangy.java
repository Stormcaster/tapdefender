package Monster;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by PrangerZ54 on 5/30/2016 AD.
 */
public class Prangy extends Monster {
    public Prangy() {
        name = "Prangy";
        damage = 20;
        curHp = maxHp = 40;
        img = R.drawable.mon_prangy;
        bounty = 30;
    }
}
