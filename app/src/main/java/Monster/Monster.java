package Monster;


/**This class is the attribures of each monster
 * Created by PrangerZ54 on 5/20/2016 AD.
 */
public abstract class Monster {
    String name;
    double curHp, damage, maxHp;
    int img, bounty;

    public int getImg() {
        return img;
    }

    public String getName() {
        return name;
    }
    public double getDamage() {
        return damage;
    }
    public void takeDamage(double damage) {
        curHp -= damage;
    }

    public double getCurHp() {
        return curHp;
    }
    public double getMaxHp() {
        return maxHp;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public void setMaxHp(double maxHp) {
        this.maxHp = maxHp;
        this.curHp = maxHp;
    }

    public void setBounty(int bounty) {
        this.bounty = bounty;
    }

    public void setCurHp(double hp) {
        this.curHp = hp;
    }

    public int getBounty() {
        return bounty;
    }
}
