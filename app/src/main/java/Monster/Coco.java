package Monster;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by PrangerZ54 on 5/30/2016 AD.
 */
public class Coco extends Monster {
    public Coco() {
        name = "Coco";
        damage = 25;
        curHp = maxHp = 60;
        img = R.drawable.mon_coco;
        bounty = 40;
    }
}
