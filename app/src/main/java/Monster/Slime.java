package Monster;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by zBlizzarDz on 29-May-16.
 */
public class Slime extends Monster {
    public Slime() {
        name = "Slime";
        damage = 10;
        curHp = maxHp = 30;
        img = R.drawable.monster_slime;
        bounty = 15;
    }
}
