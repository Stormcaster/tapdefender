package Models;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by zBlizzarDz on 29-May-16.
 */
public class Heal extends Skill {
    public Heal() {
        this.setLv(1);
        this.setMp(100);
        this.damaging = false;
        this.setPowerMult(30);
        cost = 10;
        img = R.drawable.heal;
        name = "Heal";
        desc = "Restores the castle's HP";
    }

    @Override
    public void levelUp() {
        lv++;
        powerMult = 30+2*lv;
        cost+= 10*lv;
    }
}
