package Models;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.Image;
import android.widget.ImageView;

import com.example.zblizzardz.tapdefender.MobSpawner;
import com.example.zblizzardz.tapdefender.R;
import com.example.zblizzardz.tapdefender.StartActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

import Companion.*;
import Monster.*;
/**
 * Created by zBlizzarDz on 29-May-16.
 */
public class Game extends Observable {

    Monster monster;
    Player player;
    Castle castle;
    Timer companionTimer, mobAttackTimer, mpTimer;
    List<Companion> companions;
    MobSpawner spawner;
    boolean gameOver;
    private static Game game = new Game();
    int gameLv;

    private Game() {
        player = Player.getInstance();
        gameLv = 1;
        companions = new ArrayList<Companion>();
        companions.add(new Penguin());
        companions.add(new Deer());
        spawner = new MobSpawner();
        castle = new Castle();
        gameOver = false;
    }
    public void stop() {
        companionTimer.cancel();
        mobAttackTimer.cancel();
        mpTimer.cancel();
    }
    public void start() {
        spawnMob();
        startCompanionAttack();
        startMobAttack();
        startRegenMP();
    }
    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public static Game getInstance() {
        return game;
    }
    public void applySkill(Skill skill) {
        double skillEffect = 0;
        skillEffect = player.getAtk() * skill.getPowerMult();
        if(skill.isDamaging())  {
            monster.takeDamage(skillEffect);
        }
        else {
            castle.heal(skillEffect);
        }
        check();
    }
    public boolean useSkill(Skill skill) {
        double mpCost = skill.getMp();
        double mp = player.getMp();
        if(mp >= mpCost) {
            applySkill(skill);
            player.useMp(mpCost);
            return true;
        }
        else {
            return false;
        }
    }
    public double getCastleMaxHp() {
        return castle.getMaxHp();
    }
    public double getCastleCurHp() {
        return castle.getCurHp();
    }
    public boolean useUpSkill() {
        Skill thisSkill = player.getUpSkill();
        return useSkill(thisSkill);
    }
    public boolean useLeftSkill() {
        Skill thisSkill = player.getLeftSkill();
        return useSkill(thisSkill);
    }
    public boolean useDownSkill() {
        Skill thisSkill = player.getDownSkill();
        return useSkill(thisSkill);
    }
    public boolean useRightSkill() {
        Skill thisSkill = player.getRightSkill();
        return useSkill(thisSkill);
    }
    public void useNormalAttack() {
        monster.takeDamage(player.getAtk());
        check();
    }
    private void check() {
        if(monster.getCurHp() <= 0) {
            player.setCoin(player.getCoin() + monster.getBounty());
            gameLv++;
            spawnMob();
        }
        setChanged();
        notifyObservers();

    }
    public void spawnMob() {
        monster = spawner.spawnMob(gameLv);
    }
    public int getMobImage() {
        return monster.getImg();
    }
    public Monster getMob() {
        return monster;
    }
    public void startCompanionAttack() {
        companionTimer = new Timer();
        companionTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                for(Companion companion : companions) {
                    monster.takeDamage(companion.getDps());
                    check();
                }
            }
        }, 0, 1000);
    }
    public void startMobAttack() {
        mobAttackTimer = new Timer();
        mobAttackTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                castle.takeDamage(monster.getDamage());
                if((int)game.getCastleCurHp() <= 0) {
                    gameOver = true;
                }
            }
        }, 0,1000);
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void reset() {
        player.reset();
        game = new Game();
        this.stop();
    }

    public void startRegenMP() {
        mpTimer = new Timer();
        mpTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(player.getMp() + player.getMpRegen() <= player.getMaxMp()) {
                    player.gainMp(player.getMpRegen());
                }
                else {
                    player.setMp(player.getMaxMp());
                }
                castle.applyShield(player.getShield().getMaxHpInc());
                if(castle.getCurHp() + player.getHpPotion().getHPregen() <= castle.getMaxHp()) {
                    castle.heal(player.getHpPotion().getHPregen());
                }
            }
        },0,1000);
    }
    public int getGameLv() {
        return gameLv;
    }
    public double getCompDPS() {
        double dps = 0;
        for(Companion companion : companions) {
            dps += companion.getDps();
        }
        return dps;
    }

    public List<Companion> getCompanions() {
        return companions;
    }

    public void setGameLv(int gameLv) {
        this.gameLv = gameLv;
    }

    public Castle getCastle() {
        return castle;
    }
}
