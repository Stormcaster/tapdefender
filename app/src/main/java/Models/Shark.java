package Models;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by Net on 5/31/2016.
 */
public class Shark extends Skill {
    public Shark() {
        this.setPowerMult(30);
        this.setMp(25);
        this.setLv(1);
        this.damaging = true;
        img = R.drawable.skill_icon_shark;
        cost = 10;
        name = "Sharkstrike";
        desc = "Attack with power of water";
    }

    @Override
    public void levelUp() {
        lv++;
        powerMult = 40 + lv*6;
        cost = 15*lv;
    }

}
