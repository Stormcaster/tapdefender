package Models;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by zBlizzarDz on 29-May-16.
 */
public class Lightning extends Skill {

    public Lightning() {
        this.setPowerMult(30);
        this.setMp(20);
        this.setLv(1);
        this.damaging = true;
        cost = 10;
        name = "Lightning";
        desc = "Attack with the power of lightning";
        img = R.drawable.lightning;
    }

    @Override
    public void levelUp() {
        lv++;
        powerMult = 30 + lv*5;
        cost += 10;
    }
}
