package Models;

/**
 * Created by Net on 6/1/2016.
 */
public abstract class Upgrade {
    public abstract String getName();
    public abstract String getDesc();
    public abstract int getLv();
    public abstract void levelUp();
    public abstract int getCost();
    public abstract int getImg();
    public void setLevel(int lv) {
        lv -= 1;
        for(int i = 0; i < lv; i++) levelUp();
    }
}
