package Models;

import android.content.ClipData;

import java.util.ArrayList;
import java.util.List;

import Items.*;
import Monster.Monster;

/**This class is the player class
 * Created by PrangerZ54 on 5/20/2016 AD.
 */


public class Player {

    private static Player player = new Player();
    final private double maxMp = 100;
    private double mp, atk, mpRegen;
    private int coin;
    private Skill upSkill, downSkill, leftSkill, rightSkill;
    private Sword sword;
    private Shield shield;
    private HpPotion hpPotion;
    private MpPotion mpPotion;

    private Player(){
        mp = 100;
        atk = 1;
        upSkill = null;
        downSkill = null;
        leftSkill = null;
        rightSkill = null;
        mpRegen = 2;
        sword = new Sword();
        shield = new Shield();
        hpPotion = new HpPotion();
        mpPotion = new MpPotion();
    }

    public void reset() {
        player = new Player();
    }

    public Sword getSword() {
        return sword;
    }

    public Shield getShield() {
        return shield;
    }

    public void setMp(double mp) {
        this.mp = mp;
    }

    public HpPotion getHpPotion() {
        return hpPotion;
    }

    public MpPotion getMpPotion() {
        return mpPotion;
    }

    public boolean pay(int cost) {
        if(coin - cost >= 0) {
            coin -= cost;
            return true;
        }
        else return false;
    }
    public static Player getInstance(){
        return player;
    }

    public int getCoin() {
        return coin;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }

    public double getMp() {
        return mp;
    }

    public double getAtk() {
        return atk + sword.getAtkInc();
    }

    public Skill getUpSkill() {
        return upSkill;
    }

    public Skill getDownSkill() {
        return downSkill;
    }

    public Skill getLeftSkill() {
        return leftSkill;
    }

    public Skill getRightSkill() {
        return rightSkill;
    }

    public double getMaxMp() {return maxMp; }

    public void setAtk(double atk) {
        this.atk = atk;
    }

    public void setUpSkill(Skill upSkill) {
        this.upSkill = upSkill;
    }

    public void setDownSkill(Skill downSkill) {
        this.downSkill = downSkill;
    }

    public void setLeftSkill(Skill leftSkill) {
        this.leftSkill = leftSkill;
    }

    public void setRightSkill(Skill rightSkill) {
        this.rightSkill = rightSkill;
    }

    public void useMp(double amount) {this.mp -= amount;}
    public void gainMp(double amount) {this.mp+= amount;}

    public double getMpRegen() {
        return mpRegen + mpPotion.getMpRegen();
    }
}
