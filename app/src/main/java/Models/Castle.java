package Models;

import android.util.Log;

import Items.Items;
import Monster.Monster;

/**
 * Created by PrangerZ54 on 5/20/2016 AD.
 */
public class Castle {

    private double curHp, maxHp;
    private int lv;
    final private double baseHp = 100;
    public Castle() {
        maxHp = curHp = baseHp;
        lv = 1;
    }

    //decrease hp that atacked by monster
    public void takeDamage(double damage) {
        curHp -= damage;
    }

    public double getCurHp() {
        return curHp;
    }

    public void setCurHp(double curHp) {
        this.curHp = curHp;
    }

    public double getMaxHp() {
        return maxHp;
    }

    public void setMaxHp(double maxHp) {
        this.maxHp = maxHp;
        curHp = maxHp;
    }

    public void heal(double healAmount) {
        this.curHp += healAmount;
        if(curHp > maxHp) {
            curHp = maxHp;
        }
    }
    public int getLv() {
        return lv;
    }

    public void setLv(int lv) {
        this.lv = lv;
    }

    public void applyShield(int shield) {
        if(maxHp != baseHp + shield) {
            maxHp = baseHp + shield;
            Log.e("Tag", "Shield Upgraded");
        }
        Log.e("Tag", String.format("HP: %f / %f",maxHp,curHp));
    }
}
