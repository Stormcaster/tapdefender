package Models;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by PrangerZ54 on 5/30/2016 AD.
 */
public class Genesis extends Skill {

    public Genesis() {
        this.setPowerMult(20);
        this.setMp(30);
        this.setLv(1);
        this.damaging = true;
        name = "Genesis";
        desc = "Attack with the power of light";
        cost = 30;
        img = R.drawable.genesis;
    }

    @Override
    public void levelUp() {
        lv++;
        powerMult = 20 + lv*10;
        cost = 20 + 15*lv;
    }

}
