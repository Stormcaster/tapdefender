package Models;

/**This class is attirbutes of all skills.
 * Created by PrangerZ54 on 5/20/2016 AD.
 */
public abstract class Skill extends Upgrade{
    int lv, cost, img;
    double powerMult, mpCost;
    boolean damaging;
    String name, desc;

    public boolean isDamaging() {
        return damaging;
    }

    public int getLv() {
        return lv;
    }

    public void setLv(int lv) {
        this.lv = lv;
    }

    public abstract void levelUp();
    public double getPowerMult() {
        return powerMult;
    }
    public void setPowerMult(double powerMult) {
        this.powerMult = powerMult;
    }
    public String getName() {
        return this.name;
    }
    public void setMp(double mpCost) {
        this.mpCost = mpCost;
    }
    public double getMp() {
        return this.mpCost;
    }

    @Override
    public int getCost() {
        return cost;
    }

    @Override
    public int getImg() {
        return img;
    }

    public String getDesc() {
        return desc;
    }
}
