package com.example.zblizzardz.tapdefender;

/**
 * Created by zBlizzarDz on 30-May-16.
 */
import Monster.*;
public class MobSpawner {
    int baseHP, incHP, bossInterval, mobCount, bossHpMul, bossBaseAtk, bossBaseBounty, baseAtk, atkMul, baseBounty;
    public MobSpawner() {
        baseHP = 19;
        incHP = 1;
        bossInterval = 20;
        mobCount = 7;
        bossHpMul = 2;
        bossBaseAtk = 10;
        bossBaseBounty = 15;
        baseAtk = 1;
        atkMul = 3;
        baseBounty = 2;
    }
    public Monster spawnMob(int gameLvl) {
        Monster mob = null;
        boolean isBossLevel = (gameLvl % bossInterval == 0);
        mob = randomMob();
        if(isBossLevel) {
            mob.setMaxHp(gameLvl * bossHpMul);
            mob.setDamage(10 * (gameLvl/bossInterval));
            mob.setBounty(bossBaseBounty * (gameLvl/bossInterval));
        }
        else {
            mob.setMaxHp(gameLvl + baseHP);
            mob.setDamage(baseAtk + ((gameLvl - 1)/atkMul));
            mob.setBounty(baseBounty + (gameLvl/bossInterval));
        }

        return mob;
    }
    private Monster randomMob() {
        Monster mob = null;
        int rand = (int)Math.floor(Math.random() * mobCount);
        switch(rand) {
            case 0: mob = new Slime(); break;
            case 1: mob = new Bull(); break;
            case 2: mob = new IceKnight(); break;
            case 3: mob = new Coco(); break;
            case 4: mob = new Dodo(); break;
            case 5: mob = new Prangy(); break;
            case 6: mob = new Dragon(); break;
        }
        return mob;
    }
}
