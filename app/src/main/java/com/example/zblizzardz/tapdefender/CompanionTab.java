package com.example.zblizzardz.tapdefender;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import Adapter.UpgradesAdapter;
import Companion.Companion;
import Models.Game;
import Models.Player;
import Models.Upgrade;

public class CompanionTab extends Activity implements Observer{

    private ListView companionList;
    private UpgradesAdapter upgradesAdapter;
    private Game game = Game.getInstance();
    private TextView coinTxt;
    private Player ply = Player.getInstance();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_companion_tab);
        coinTxt = (TextView)findViewById(R.id.coinTxt);
        game.addObserver(this);
        initComponents();
    }

    private void initComponents() {
        companionList = (ListView)findViewById(R.id.companionList);
        List<Upgrade> companions = new ArrayList<Upgrade>();
        for(Companion comp : game.getCompanions()) {
            companions.add(comp);
        }
        upgradesAdapter = new UpgradesAdapter(this, R.layout.upgrades_cell, companions);
        companionList.setAdapter(upgradesAdapter);
    }

    @Override
    public void update(Observable observable, Object data) {
        coinTxt.post(new Runnable() {
            @Override
            public void run() {
                coinTxt.setText(ply.getCoin() + "");
            }
        });

    }
}
