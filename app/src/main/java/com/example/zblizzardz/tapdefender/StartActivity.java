package com.example.zblizzardz.tapdefender;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import Models.Game;

public class StartActivity extends AppCompatActivity {

    private Button startButton;
    private Button instructionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        startButton = (Button)findViewById(R.id.startBtn);
        instructionButton = (Button)findViewById(R.id.instructionBtn);

        initComponents();
    }

    private void initComponents() {
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        instructionButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(StartActivity.this, InstructionActivity.class);
                startActivity(intent);
            }
        });

    }
}
