package com.example.zblizzardz.tapdefender;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import Adapter.UpgradesAdapter;
import Items.Items;
import Models.Game;
import Models.Player;
import Models.Upgrade;

public class ItemTab extends Activity implements Observer {

    private ListView itemList;
    private List<Upgrade> items;
    private Player ply = Player.getInstance();
    private Game game = Game.getInstance();
    private UpgradesAdapter upgradesAdapter;
    private TextView coinTxt;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_tab);
        coinTxt = (TextView)findViewById(R.id.coinTxt);
        game.addObserver(this);
        initComponents();
    }

    private void initComponents() {
        itemList = (ListView)findViewById(R.id.itemList);
        items = new ArrayList<Upgrade>();
        items.add(ply.getSword());
        items.add(ply.getShield());
        items.add(ply.getHpPotion());
        items.add(ply.getMpPotion());
        upgradesAdapter = new UpgradesAdapter(this, R.layout.upgrades_cell, items);
        itemList.setAdapter(upgradesAdapter);
    }

    @Override
    public void update(Observable observable, Object data) {
        coinTxt.post(new Runnable() {
            @Override
            public void run() {
                coinTxt.setText(ply.getCoin() + "");
            }
        });

    }

}
