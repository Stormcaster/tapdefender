package com.example.zblizzardz.tapdefender;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import Models.Game;
import Models.Genesis;
import Models.Heal;
import Models.Player;
import Models.Lightning;
import Models.Shark;

public class MissionTab extends Activity implements Observer {
    /**
     * Called when the activity is first created.
     */
    ImageView mobImg;
    boolean newGame = true;
    String myPrefs = "MYPREFS";
    SharedPreferences sp;
    private double mobHP;
    private ImageView skillGenesis, skillHeal, skillLight, skillShark, skillTap;
    private ImageView battleSpace;
    TextView coinTxt, lvTxt, playerAtk, compDPS;
    ProgressBar mobHpBar, HPBar, MPBar;
    Player player;
    Game game;
    final Context context = this;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mission_tab);
        sp = getSharedPreferences(myPrefs,MODE_PRIVATE);

        coinTxt = (TextView) this.findViewById(R.id.coinTxt);
        lvTxt = (TextView) this.findViewById(R.id.lvCount);
        playerAtk = (TextView) this.findViewById(R.id.playerAtkTxt);
        compDPS = (TextView) this.findViewById(R.id.compDPSTxt);
        battleSpace = (ImageView) this.findViewById(R.id.battleSpace);
        //Skill
        skillLight = (ImageView) this.findViewById(R.id.skill_light_img);
        skillHeal = (ImageView) this.findViewById(R.id.skill_heal_img);
        skillGenesis = (ImageView) this.findViewById(R.id.skill_genesis_img);
        skillShark = (ImageView) this.findViewById(R.id.skill_shark_img);
        skillTap = (ImageView) this.findViewById(R.id.skill_tap_img);
        //Monster
        mobImg = (ImageView) this.findViewById(R.id.mobImg);
        mobHpBar = (ProgressBar) this.findViewById(R.id.mobHp);
        HPBar = (ProgressBar)this.findViewById(R.id.HPBar);
        MPBar = (ProgressBar)this.findViewById(R.id.MPBar);
        mobHP = 100;
        player = Player.getInstance();
        player.setUpSkill(new Heal());
        player.setDownSkill(new Genesis());
        player.setLeftSkill(new Lightning());
        player.setRightSkill(new Shark());
        game = Game.getInstance();

        if(sp.contains("castleCurHP")) {

            if(Double.parseDouble(sp.getString("castleCurHP", "100")) > 0) {
                game.getCastle().setCurHp(Double.parseDouble(sp.getString("castleCurHP", "100")));
                newGame = false;
            }
        }
        if(!newGame) {
            if (sp.contains("coin")) player.setCoin(sp.getInt("coin", 0));
            if (sp.contains("playerMp"))
                player.setMp(Double.parseDouble(sp.getString("playerMp", "0")));

            if (sp.contains("skillUpLv")) player.getUpSkill().setLv(sp.getInt("skillUpLv", 1));

            if (sp.contains("skillDownLv"))
                player.getDownSkill().setLv(sp.getInt("skillDownLv", 1));

            if (sp.contains("skillLeftLv"))
                player.getLeftSkill().setLv(sp.getInt("skillLeftLv", 1));

            if (sp.contains("skillRightLv"))
                player.getRightSkill().setLv(sp.getInt("skillRightLv", 1));

            if (sp.contains("swordLv")) player.getSword().setLevel(sp.getInt("swordLv", 1));
            if (sp.contains("shieldLv")) player.getShield().setLevel(sp.getInt("shieldLv", 1));
            if (sp.contains("hpPotionLv")) player.getHpPotion().setLevel(sp.getInt("hpPotionLv", 1));
            if (sp.contains("mpPotionLv")) player.getMpPotion().setLevel(sp.getInt("mpPotionLv", 1));
            if(sp.contains("gameLv")) game.setGameLv(sp.getInt("gameLv", 1));
            if(sp.contains("compPenguinLv")) game.getCompanions().get(0).setLv(sp.getInt("compPenguinLv", 1));
            if(sp.contains("compDeerLv")) game.getCompanions().get(1).setLv(sp.getInt("compDeerLv", 1));

        }




        game.start();
        game.addObserver(this);
        myPrefs = "MyPrefs";
        battleSpace.setOnTouchListener(new OnSwipeTouchListener(MissionTab.this) {
            public void onSwipeTop() {
                if(game.useUpSkill()) {
                    skillHeal.post(new Runnable() {
                        @Override
                        public void run() {
                            ((AnimationDrawable) skillHeal.getBackground()).stop();
                            ((AnimationDrawable) skillHeal.getBackground()).start();
                        }
                    });
                    refresh();
                }
            }

            public void onSwipeRight() {
                if(game.useRightSkill()) {
                    skillShark.post(new Runnable() {
                        @Override
                        public void run() {
                            ((AnimationDrawable) skillShark.getBackground()).stop();
                            ((AnimationDrawable) skillShark.getBackground()).start();
                        }
                    });
                    refresh();
                }
            }

            public void onSwipeLeft() {
                if(game.useLeftSkill()) {
                    skillLight.post(new Runnable() {
                        @Override
                        public void run() {
                            ((AnimationDrawable) skillLight.getBackground()).stop();
                            ((AnimationDrawable) skillLight.getBackground()).start();
                        }
                    });
                    refresh();
                }
            }

            public void onSwipeBottom() {
                if(game.useDownSkill()) {
                    skillGenesis.post(new Runnable() {
                        @Override
                        public void run() {
                            ((AnimationDrawable) skillGenesis.getBackground()).stop();
                            ((AnimationDrawable) skillGenesis.getBackground()).start();
                        }
                    });
                    refresh();
                }
            }

            public void onTap() {
                skillTap.post(new Runnable() {
                    @Override
                    public void run() {
                        ((AnimationDrawable) skillTap.getBackground()).stop();
                        ((AnimationDrawable) skillTap.getBackground()).start();
                    }
                });
                game.useNormalAttack();
                refresh();
            }
        });

    }

    private void refresh() {
        HPBar.post(new Runnable() {
            @Override
            public void run() {
                HPBar.setMax((int)game.getCastleMaxHp());
                HPBar.setProgress((int)game.getCastleCurHp());

            }
        });
        MPBar.post(new Runnable() {
            @Override
            public void run() {
                MPBar.setMax((int)player.getMaxMp());
                MPBar.setProgress((int)player.getMp());
            }
        });
        mobHpBar.post(new Runnable() {
            @Override
            public void run() {
                mobHpBar.setMax((int) game.getMob().getMaxHp());
                mobHpBar.setProgress((int) game.getMob().getCurHp());
            }
        });
        coinTxt.post(new Runnable() {
            @Override
            public void run() {
                coinTxt.setText(String.format("%d", player.getCoin()));
            }
        });
        mobImg.post(new Runnable() {
            @Override
            public void run() {
                mobImg.setImageResource(game.getMobImage());
            }
        });
        lvTxt.post(new Runnable() {
            @Override
            public void run() {
                lvTxt.setText(game.getGameLv() + "");
            }
        });
        playerAtk.post(new Runnable() {
            @Override
            public void run() {
                playerAtk.setText(player.getAtk() + "");
            }
        });
        compDPS.post(new Runnable() {
            @Override
            public void run() {
                compDPS.setText(game.getCompDPS() + "");
            }
        });
    }

    @Override
    public void update(Observable observable, Object data) {
//        if(data == null) data = "";
//        String msg = (String)data;
//        if(msg.equals("Gameover")) {
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle("Game Over");
//            builder.setMessage("You failed me!");
//            builder.setPositiveButton("OK",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog,
//                                            int which) {
//                        }
//                    });
//            builder.show();
//        }
        if(game.isGameOver()) {
            game.setGameOver(false);
            game.reset();
            Intent intent = new Intent(MissionTab.this, StartActivity.class);
            startActivity(intent);
            //Toast.makeText(getApplicationContext(),"Game Over",Toast.LENGTH_LONG).show();
        }
        refresh();

    }

    @Override
    protected void onStop() {
        SharedPreferences.Editor edit = sp.edit();

        edit.putInt("gameLv", game.getGameLv());
        edit.putString("castleCurHP", game.getCastleCurHp() + "");
        edit.putString("playerMp", player.getMp() + "");
        edit.putInt("coin", player.getCoin());
        edit.putInt("shieldLv", player.getShield().getLv());
        edit.putInt("swordLv", player.getSword().getLv());
        edit.putInt("hpPotionLv", player.getHpPotion().getLv());
        edit.putInt("mpPotionLv", player.getMpPotion().getLv());
        edit.putInt("skillUpLv", player.getUpSkill().getLv());
        edit.putInt("skillDownLv", player.getDownSkill().getLv());
        edit.putInt("skillLeftLv", player.getLeftSkill().getLv());
        edit.putInt("skillRightLv", player.getRightSkill().getLv());
        edit.putInt("compPenguinLv", game.getCompanions().get(0).getLv());
        edit.putInt("compDeerLv", game.getCompanions().get(1).getLv());
        edit.commit();


        super.onStop();
    }

}
