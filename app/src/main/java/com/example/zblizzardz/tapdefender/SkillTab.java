package com.example.zblizzardz.tapdefender;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import Adapter.UpgradesAdapter;
import Models.Game;
import Models.Player;
import Models.Skill;
import Models.Upgrade;

public class SkillTab extends Activity implements Observer {

    private ListView skillList;
    private UpgradesAdapter upgradesAdapter;
    private List<Upgrade> skills;
    private Player player = Player.getInstance();
    private TextView coinTxt;
    Game game;
    Player ply = Player.getInstance();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skill_tab);
        game = Game.getInstance();
        coinTxt = (TextView)findViewById(R.id.coinTxt);
        initComponents();
        game.addObserver(this);
    }

    private void initComponents() {
        skills = new ArrayList<Upgrade>();
        skills.add(player.getDownSkill());
        skills.add(player.getLeftSkill());
        skills.add(player.getRightSkill());
        skills.add(player.getUpSkill());
        skillList = (ListView)findViewById(R.id.skillList);
        upgradesAdapter = new UpgradesAdapter(this, R.layout.upgrades_cell, skills);
        skillList.setAdapter(upgradesAdapter);
    }


    @Override
    public void update(Observable observable, Object data) {
    coinTxt.post(new Runnable() {
        @Override
        public void run() {
            coinTxt.setText(ply.getCoin() + "");
        }
    });
    }
}
