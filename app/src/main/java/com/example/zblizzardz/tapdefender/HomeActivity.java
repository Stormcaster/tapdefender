package com.example.zblizzardz.tapdefender;

import android.app.TabActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class HomeActivity extends TabActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        TabHost tabHost = getTabHost();

        // Mission Tab
        TabSpec missionSpec = tabHost.newTabSpec("Mission");
        missionSpec.setIndicator("Mission");
        Intent missionIntent = new Intent(this, MissionTab.class);
        missionSpec.setContent(missionIntent);

        // Item Tab
        TabSpec itemSpec = tabHost.newTabSpec("Item");
        itemSpec.setIndicator("Item");
        Intent itemIntent = new Intent(this, ItemTab.class);
        itemSpec.setContent(itemIntent);

        // Skill Tab
        TabSpec skillSpec = tabHost.newTabSpec("Skill");
        skillSpec.setIndicator("Skill");
        Intent skillIntent = new Intent(this, SkillTab.class);
        skillSpec.setContent(skillIntent);

        //Companion Tab
        TabSpec companionSpec = tabHost.newTabSpec("Companion");
        companionSpec.setIndicator("Companion");
        Intent companionIntent = new Intent(this, CompanionTab.class);
        companionSpec.setContent(companionIntent);

        // Adding all TabSpec to TabHost
        tabHost.addTab(missionSpec);
        tabHost.addTab(itemSpec);
        tabHost.addTab(skillSpec);
        tabHost.addTab(companionSpec);
    }


}
