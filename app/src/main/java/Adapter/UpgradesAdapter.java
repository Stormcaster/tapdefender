package Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zblizzardz.tapdefender.R;

import org.w3c.dom.Text;

import java.util.List;

import Companion.Companion;
import Models.Game;
import Models.Player;
import Models.Upgrade;

/**
 * Created by Net on 6/1/2016.
 */
public class UpgradesAdapter extends ArrayAdapter<Upgrade>{
    Player ply = Player.getInstance();
    public UpgradesAdapter(Context context, int resource, List<Upgrade> object) {
        super(context, resource, object);
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v == null) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.upgrades_cell, null);
        }

        ImageView upgImg = (ImageView)v.findViewById(R.id.upgradesImg);
        upgImg.setImageResource(getItem(position).getImg());
        final TextView lvTxt = (TextView)v.findViewById(R.id.levelTxt);
        final Button btn = (Button)v.findViewById(R.id.upgradeBtn);
        btn.setText(getItem(position).getCost() + "");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Tag", "" + getItem(position).getCost());
                if(ply.pay(getItem(position).getCost())) {
                    Log.e("Tag", "Paid for " + getItem(position).getName());
                    getItem(position).levelUp();
                    btn.setText(getItem(position).getCost() + "");
                    lvTxt.setText("Lv : " + getItem(position).getLv());
                }
            }
        });
        TextView txtView = (TextView)v.findViewById(R.id.upgradeTxt);
        txtView.setText(getItem(position).getName());
        lvTxt.setText("Lv : " + getItem(position).getLv());
        btn.setText(getItem(position).getCost() + "");


        return v;
    }
}
