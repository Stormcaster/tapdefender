package Companion;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by zBlizzarDz on 01-Jun-16.
 */
public class Deer extends Companion {

    public Deer() {
        name = "Deer";
        cost = 1;
        lv = 1;
        dps = 0.5;
        img = R.drawable.comp_deer;
    }

    @Override
    public void levelUp() {
        lv++;
        dps = 0.5 + 0.5*lv;
        cost = 2*lv;

    }
}
