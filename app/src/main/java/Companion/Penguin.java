package Companion;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by zBlizzarDz on 29-May-16.
 */
public class Penguin extends Companion {
    public Penguin() {
        name = "Penguin";
        dps = 0.2;
        lv = 1;
        cost = 1;
        img = R.drawable.comp_penguin;
    }

    @Override
    public void levelUp() {
        lv++;
        dps = 0.2 + 0.3*lv;
        cost = lv;

    }
}
