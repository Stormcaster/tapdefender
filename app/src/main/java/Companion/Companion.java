package Companion;

import Models.Upgrade;

/**
 * Created by zBlizzarDz on 29-May-16.
 */
public abstract class Companion extends Upgrade{
    int lv, cost;
    double dps;
    String name, desc;
    //image from drawable
    int img;

    public abstract void levelUp();
    public String getName() {
        return name;
    }

    public double getDps() {
        return dps;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    @Override
    public int getCost() {
        return cost;
    }

    public void setDps(double dps) {
        this.dps = dps;
    }

    public int getLv() {
        return lv;
    }

    public void setLv(int lv) {
        this.lv = lv;
    }

    public int getImg() {
        return img;
    }

}
