package Items;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by Net on 5/28/2016.
 */
public class Sword extends Items{
    int atkInc;

    public Sword() {
        atkInc = 1;
        lv = 1;
        cost = 1;
        name = "Sword";
        desc = "Increase base ATK";
        img = R.drawable.maple_sword1;
    }

    @Override
    public void levelUp() {
        lv++;
        atkInc = lv;
        cost = lv;
    }

    public int getAtkInc() {
        return atkInc;
    }
}
