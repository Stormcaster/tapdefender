package Items;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by Net on 5/28/2016.
 */
public class Shield extends Items {
    int maxHpInc;

    public Shield() {
        this.maxHpInc = 100;
        lv = 1;
        name = "Shield";
        desc = "Increase Max HP of the castle";
        cost = lv;
        img = R.drawable.maple_shield1;
    }

    @Override
    public void levelUp() {
        lv++;
        cost = lv;
        maxHpInc = 100*lv;
    }

    public int getMaxHpInc() {
        return maxHpInc;
    }
}
