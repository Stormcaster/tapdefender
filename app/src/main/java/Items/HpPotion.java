package Items;


import com.example.zblizzardz.tapdefender.R;

/**
 * Created by PrangerZ54 on 5/27/2016 AD.
 */

public class HpPotion extends Items {
    double HPregen;

    public HpPotion() {
        name = "HP Alchemy";
        desc = "Increases HP Regeneration of the castle";
        img = R.drawable.red_potion;
        lv = 1;
        cost = 1;
        HPregen = 0.5;
    }

    @Override
    public void levelUp() {
        lv++;
        HPregen = lv*0.5;
        cost = lv;
    }

    public double getHPregen() {
        return HPregen;
    }
}
