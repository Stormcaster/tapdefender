package Items;

import Models.Upgrade;

/**This class ia about attributes of each Items
 * Created by PrangerZ54 on 5/20/2016 AD.
 */
public abstract class Items extends Upgrade{
    String name, desc;
    int lv, cost, img;
    public String getName() {return name;}
    public String getDesc(){return desc;}

    @Override
    public int getLv() {
        return lv;
    }

    @Override
    public abstract void levelUp();

    @Override
    public int getCost() {
        return cost;
    }

    @Override
    public int getImg() {
        return img;
    }
}
