package Items;

import com.example.zblizzardz.tapdefender.R;

/**
 * Created by Net on 5/28/2016.
 */
public class MpPotion extends Items {
    int mpRegen;

    public MpPotion() {
        name = "Mana Alchemy";
        desc = "Increase your MP regeneration";
        lv = 1;
        mpRegen = 2;
        cost = 10;
        img = R.drawable.blue_potion;
    }

    @Override
    public void levelUp() {
        lv++;
        mpRegen = lv;
        cost = 10+(50*(lv-1));
    }

    public int getMpRegen() {
        return mpRegen;
    }
}
